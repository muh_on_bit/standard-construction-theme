<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta charset="UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="https://fonts.googleapis.com/css2?family=IBM+Plex+Sans:wght@300;400;500;600;700&family=Josefin+Sans:wght@300;400;500;600;700&family=Teko:wght@300;400;500;600;700&display=swap" rel="stylesheet">
	<link rel="preconnect" href="https://fonts.gstatic.com">

	<?php wp_head(); ?>

</head>

<?php
$browser = new Wolfcast\BrowserDetection();
if ($browser->getPlatform() == Wolfcast\BrowserDetection::PLATFORM_IOS) :
	$ios = 'ios';
else :
	$ios = '';
endif;
?>

<body <?php body_class($ios); ?>>
	<div id="page" class="site inner">
		<?php do_action('before'); ?>
		<header id="masthead" class="site-header pt-lg-4 inner" style="background-image: url(<?= !is_page(1) ? get_the_post_thumbnail_url() : ''; ?>);background-color: #1f1f30;">
			<div class="innerin nav_wrap container-fluid d-flex flex-lg-column flex-xl-row justify-content-between align-items-center">
				<div id="logo">
					<a href="<?php echo home_url('/'); ?>" title="<?php echo esc_attr(get_bloginfo('name', 'display')); ?>" rel="home">
						<span><?php bloginfo('name'); ?></span>
						<?php get_template_part('template-parts/components/content', 'logo'); ?>
					</a>
				</div>

				<div class="d-lg-none" id="mobile-menu">
					<div id="nav-icon2">
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
						<span></span>
					</div>
				</div>
				<nav id="nav" class="site-navigation mt-lg-4 mt-xl-0 main-navigation">

					<div class="sr-only sr-only-focusable"><a href="#content" title="Skip to content" rel="nofollow">Skip to content</a></div>

					<?php wp_nav_menu(array('theme_location' => 'primary')); ?>
				</nav><!-- .site-navigation .main-navigation -->
			</div>
			<?php if (is_page(1)) { ?>
				<div>
				<img class="img-fluid d-sm-none" src="<?= IMAGES; ?>/home/hero_mobile.jpg" alt="">
			</div>
			<div class="innerin hero_cont mt-md-5 pb-5 pb-lg-0">
				<h1 class="font-teko font-regular text-white"><?= get_field('title', 'option'); ?></h1>
				<a href="<?= get_field('cta_title_link', 'option'); ?>"><h2 class="font-teko font-bold text-uppercase mt-3 bg_red text-white pt-2 pt-lg-4 pb-lg-2 px-3"><?= get_field('cta_title', 'option'); ?></h2></a>
			</div>

			<div class="bg_black d-md-none container-fluid pt-4 pb-5 pt-sm-0">
				<hr class="m-0 d-sm-none" style="border-color: #515173;" />
			</div>

			<div class="hero_blurb">
				<div class="innerin container-fluid">
					<div class="row align-items-center">
						<?php if (have_rows('hero_content', 'option')) : ?>
							<?php while (have_rows('hero_content', 'option')) : the_row(); ?>

								<div class="col-md-6 title_wrap pr-md-5">
									<h3 class="font-teko ml-lg-5 pr-4 pr-md-0 pb-5 pb-md-0 mb-0 text-uppercase font-semibold font-60 text-white text-md-right">
										<span class="text_white"><?= get_sub_field('title_white'); ?></span>
										<span class="text_red"><?= get_sub_field('title_red'); ?></span>
									</h3>
								</div>

								<div class="col-md-6 bg_white py-5 p-md-5">
									<div class="pl-md-3">
										<p class="font-20 text_gray"><?= get_sub_field('content'); ?></p>
										<a class="plus_link" href="<?= get_sub_field('link'); ?>">
											<?= get_sub_field('link_text'); ?>
										</a>
									</div>
								</div>

							<?php endwhile; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php } else { ?>
				<div class="sub_hero container-fluid innerin">
					<?php
						if(is_archive()): ?>
					   	<h1 class="mb-0 "><?php $mypost_type = post_type_archive_title(); ?></h1>
					    <?php else: ?>
					    <h1 class="mb-0"> <?php the_title(); ?></h1>
					    <?php endif; ?>
				</div>
			<?php } ?>
		</header><!-- #masthead .site-header -->

		<div id="main">