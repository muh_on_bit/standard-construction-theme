<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */

get_header(); ?>

	<div id="primary tesing" class="site-content">
		<div id="content" role="main">

			<?php // get_template_part( 'template-parts/post/content', 'none' ); ?>

			<section>
				<?php if (have_rows('404_page', 'option')) : ?>
						<?php while (have_rows('404_page', 'option')) : the_row(); ?>
							<div class="my-5 py-5 text-center">
								<h2 class="font-teko text-uppercase font-bold font-60"><span class="text_black"><?= get_sub_field('404_page_title'); ?></span></h2>
								<p><?= get_sub_field('404_page_description'); ?></p>
								<a class="button btn_red mt-4" href="<?php echo home_url(); ?>">Back To Home</a>
							</div>
							<div>
								<img src="<?= get_sub_field('404_page_image'); ?>" width="100%">
							</div>
							
						<?php endwhile; ?>
				<?php endif; ?>
			</section>

		</div><!-- #content -->
	</div><!-- #primary .site-content -->

<?php get_footer(); ?>