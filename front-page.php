<?php

/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */

get_header(); ?>
<div id="primary" class="site-content">
	<div id="content" role="main">

		<?php while (have_posts()) : the_post(); ?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
				<div class="safety_section inner">
					<div class="container-fluid innerin">
						<div class="row">
							<?php if (have_rows('safety_section')) : ?>
								<?php while (have_rows('safety_section')) : the_row(); ?>

									<div class="col-md-6">
										<h3 class="font-teko pb-5 mb-0 text-uppercase font-semibold font-60">
											<span class="text_red"><?= get_sub_field('title_red'); ?></span>
											<span class="text-white"><?= get_sub_field('title_white'); ?></span>
										</h3>

										<div class="bg-white outer_white py-5 pr-xl-5 mr-xl-5">
											<p class="pr-lg-5 font-20 text_gray"><?= get_sub_field('text'); ?></p>
											<a class="plus_link" href="<?= get_sub_field('link'); ?>">
												<?= get_sub_field('link_text'); ?>
											</a>
										</div>
									</div>

									<div class="col-md-6 d-none d-md-block">
										<img src="<?= get_sub_field('image')['url']; ?>" alt="<?= get_sub_field('image')['alt']; ?>">
									</div>

								<?php endwhile; ?>
							<?php endif; ?>
						</div>

						<div class="container py-5">
							<div class="brands_slider my-5">
								<?php if (have_rows('brands_slider')) : ?>
									<?php while (have_rows('brands_slider')) : the_row(); ?>
										<div class="d-flex px-lg-4 slide_wrap justify-content-center align-items-center">
											<a target="<?= get_sub_field('link')['target']; ?>" rel="noreferrer noopener" href="<?= get_sub_field('link')['url']; ?>">
												<img class="img-fluid" src="<?= get_sub_field('image')['url']; ?>" alt="<?= get_sub_field('image')['alt']; ?>">
											</a>
										</div>

									<?php endwhile; ?>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>

				<?php get_template_part( 'template-parts/components/content', 'capabilities-section' ); ?>

				<?php get_template_part( 'template-parts/components/content', 'locations-section' ); ?>

				<div class="careers_section py-5 inner">
					<div class="container-fluid my-md-3 py-5 innerin">
						<div class="row py-5">
							<?php if (have_rows('careers')) : ?>
								<?php while (have_rows('careers')) : the_row(); ?>

									<div class="col-md-8 col-lg-6">
										<h1 class="font-teko pr-xl-5 mr-xl-5 text-uppercase pt-md-5 font-72 font-bold">
											<span class="text_black"><?= get_sub_field('title_black'); ?></span>
											<span class="text_red"><?= get_sub_field('title_red'); ?></span>
										</h1>
										<p class="py-4">
										<?= get_sub_field('text'); ?>
										</p>
										<a class="button btn_red" href="<?= get_sub_field('button_link'); ?>"><?= get_sub_field('button_text'); ?></a>
									</div>

								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>

				<div class="cta_section py-5 inner">
					<div class="container-fluid my-3 py-md-5 innerin">
						<div class="row justify-content-lg-end py-4 py-md-5">
							<?php if (have_rows('cta')) : ?>
								<?php while (have_rows('cta')) : the_row(); ?>

									<div class="col-md-9 col-lg-8 col-xl-6">
										<h1 class="font-teko text-white pr-xl-2 text-uppercase font-72 font-bold">
											<?= get_sub_field('title'); ?>
										</h1>
										<p class="py-4 text-white">
										<?= get_sub_field('text'); ?>
										</p>
										<a class="button px-5 outlined_white" href="<?= get_sub_field('button_link'); ?>"><?= get_sub_field('button_text'); ?></a>
									</div>

								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
				</div>

			</article><!-- #post-<?php the_ID(); ?> -->

		<?php endwhile; // end of the loop. 
		?>

	</div><!-- #content -->
</div><!-- #primary .site-content -->
<?php get_footer(); ?>