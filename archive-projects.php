<?php

get_header(); ?>

<div id="primary" class="site-content">
  <div id="content" role="main">

    <?php // while ( have_posts() ) : the_post(); 

          get_template_part('template-parts/page/content','projects');

 	?>

    <?php // endwhile; // end of the loop. ?>

  </div><!-- #content -->
</div><!-- #primary .site-content -->

<?php get_footer(); ?>