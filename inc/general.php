<?php
define( 'TEMPPATH', get_template_directory_uri());
define( 'IMAGES', TEMPPATH. "/assets/images");
$http_host = $_SERVER['HTTP_HOST'];
$localhost = strpos( $http_host, "localhost" );
$dev = strpos( $http_host, "dev." );
if ( $localhost !== false || $dev !== false ) {
	$dev = true;
} else {
	$dev = false;
}
define( 'LOCALHOST', $dev );


/*================================
	=Remove Notifications
==================================*/
add_action( 'after_setup_theme', 'bs_setup' );
function bs_setup() {
	add_theme_support( 'title-tag' );
}

/*======================
	=Excerpt
========================*/
function excerpt($limit) {
	$excerpt = explode(' ', get_the_excerpt(), $limit);
	if (count($excerpt)>=$limit) {
	array_pop($excerpt);
	$excerpt = implode(" ",$excerpt).'...';
	} else {
	$excerpt = implode(" ",$excerpt);
	}
	$excerpt = preg_replace('`[[^]]*]`','',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	return $excerpt;
}

add_action( 'after_setup_theme', 'bigsplash_setup' );
if ( ! function_exists( 'bigsplash_setup' ) ):
	function bigsplash_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );

		if ( function_exists( 'add_image_size' ) ) {
			//add_image_size( 'mobile-slide', 320, 280, true ); //(cropped)
		}
		register_nav_menus( array(
			'primary' => 'Primary Menu',
			'company' => 'Footer Company Menu',
			'capabilities' => 'Footer Capabilities Menu',
			'offices' => 'Footer Offices Menu'
		) );
	}
endif;

// Remove Short Links
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
remove_action('wp_head', 'start_post_rel_link');
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'adjacent_posts_rel_link');

//Remove JQuery migrate
function isa_remove_jquery_migrate( &$scripts) {
    if(!is_admin()) {
        $scripts->remove( 'jquery');
        $scripts->add( 'jquery', false, array( 'jquery-core' ), '1.12.4' );
    }
}
add_filter( 'wp_default_scripts', 'isa_remove_jquery_migrate' );

function bs_deregister_dashicons()    {
	if (current_user_can( 'edit_others_pages' )) {
		 return;
	 }
	wp_deregister_style('dashicons');
}
add_action( 'wp_print_styles', 'bs_deregister_dashicons', 100 );

function remove_query_strings() {
   if(!is_admin()) {
       add_filter('script_loader_src', 'remove_query_strings_split', 15);
       add_filter('style_loader_src', 'remove_query_strings_split', 15);
   }
}

function remove_query_strings_split($src){
   $output = preg_split("/(&ver|\?ver)/", $src);
   return $output[0];
}
add_action('init', 'remove_query_strings');

if( function_exists('acf_add_options_page') ) {
	acf_add_options_page(array(
		'page_title' 	=> 'Home Hero Content',
		'menu_title'	=> 'Home Hero',
		'menu_slug' 	=> 'home-hero-content',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));
}

//add_filter('show_admin_bar', '__return_false'); //remove admin bar
?>
