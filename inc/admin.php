<?php
/*======================
	=Login
========================*/
function loginpage_custom_link() {
	return site_url();
}
add_filter('login_headerurl','loginpage_custom_link');

add_filter("login_redirect", "bs_login_redirect", 10, 3);
function bs_login_redirect( $redirect_to, $request, $user ){
	if ( isset( $_SERVER['HTTP_REFERER'] ) ) {
		$url = $_SERVER['HTTP_REFERER'];
		$parts = parse_url($url);
		if ( isset( $parts['query'] ) ) {
			parse_str($parts['query'], $query);
			if ( isset( $query['redirect_to'] ) ) {
				return $redirect_to;
			}
		}
	}
    if( isset( $user->caps ) ) {
		$user_role = key( $user->caps );
		switch( $user_role ) {
			case "administrator":
				 return $redirect_to;
				break;
			case "editor":
				return admin_url("edit.php?post_type=page");
				break;
			default:
				return site_url();
		}
    } else {
        return $redirect_to;
    }
}

/*================================
	=Remove Notifications
==================================*/
add_action( 'admin_head', 'hide_update_notice_to_all_but_admin_users', 1 );
function hide_update_notice_to_all_but_admin_users()
{
    if (!current_user_can('update_core')) {
        remove_action( 'admin_notices', 'update_nag', 3 );
    }
}

/*=========================
	=Admin
===========================*/
add_action('admin_init', 'bs_admin');
function bs_admin() {
	/* Clear WP Cache when saving ACF */

	if ( is_plugin_active( 'advanced-custom-fields-pro/acf.php' ) ) {
		add_action('acf/save_post', 'clear_cache_on_options_save');
		function clear_cache_on_options_save($post_id) {
			if ($post_id == 'options') {
				if ( function_exists( 'wp_cache_clear_cache' ) ) {
					wp_cache_clear_cache();
				}
			}
		}
	}
}

/* Remove admin menu items */
function big_splash_admin_theme_style() {
    wp_enqueue_style('big-splash-admin-theme', TEMPPATH.'/assets/css/big-splash-admin.css');
}
add_action('admin_enqueue_scripts', 'big_splash_admin_theme_style');
add_action('login_enqueue_scripts', 'big_splash_admin_theme_style');
add_editor_style( '/assets/css/big-splash-editor.css' );

add_action( 'admin_menu', 'my_remove_admin_menus', 110 );
function my_remove_admin_menus() {
    remove_menu_page( 'edit-comments.php' );
    global $current_user;
    //global $submenu;
    //echo '<pre>'.print_r($submenu,1).'</pre>';
    if(is_array($current_user->roles)) {
        if(in_array('editor', $current_user->roles)) {
			remove_menu_page( 'index.php' ); //dashboard
			remove_menu_page( 'tools.php' );
			remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');
			remove_submenu_page( 'themes.php', 'widgets.php' );
			remove_submenu_page( 'themes.php', 'themes.php' );
			remove_submenu_page( 'themes.php', 'customize.php?return=%2Fwp-admin%2Fnav-menus.php' );
        }
    }
}

function annointed_admin_bar_remove() {
        global $wp_admin_bar;
        //echo "<pre>".print_r($wp_admin_bar,1)."</pre>";
        /* Remove their stuff */
        $wp_admin_bar->remove_menu('wp-logo');
        $wp_admin_bar->remove_menu('comments');
        $wp_admin_bar->remove_node( 'new-post' );
	    $wp_admin_bar->remove_node( 'new-link' );
	    $wp_admin_bar->remove_node( 'new-media' );
	    $wp_admin_bar->remove_node( 'new-page' );
	    $wp_admin_bar->remove_node( 'new-user' );
	    $wp_admin_bar->remove_node( 'customize' );
	    $wp_admin_bar->remove_node( 'new-content' );
}
add_action( 'admin_bar_menu', 'annointed_admin_bar_remove', 999 );

//Control Hearbeat Frequency
function bs_heartbeat_Freq($settings) {
	$settings['interval'] = 60;
	return $settings;
}
add_filter( 'heartbeat_settings', 'bs_heartbeat_Freq' );

function remove_dashboard_widgets() {
    global $wp_meta_boxes;
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_quick_press']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_incoming_links']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_right_now']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_plugins']);
    //unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_activity']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_drafts']);
    unset($wp_meta_boxes['dashboard']['normal']['core']['dashboard_recent_comments']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_primary']);
    unset($wp_meta_boxes['dashboard']['side']['core']['dashboard_secondary']);
}
add_action('wp_dashboard_setup', 'remove_dashboard_widgets' );

/* Remove Yoast SEO Dashboard Widget */
add_action('wp_dashboard_setup', 'remove_wpseo_dashboard_overview' );
function remove_wpseo_dashboard_overview() {
  // In some cases, you may need to replace 'side' with 'normal' or 'advanced'.
  remove_meta_box( 'wpseo-dashboard-overview', 'dashboard', 'side' );
}

// Admin footer modification
function remove_footer_admin () {
    echo '<span id="footer-thankyou">Developed by <a href="https://www.bigsplashwebdesign.com" target="_blank">Big Splash Web Design & Marketing</a></span>';
}
add_filter('admin_footer_text', 'remove_footer_admin');

// replace WordPress Howdy
function replace_howdy( $wp_admin_bar ) {
    $my_account=$wp_admin_bar->get_node('my-account');
    $newtitle = str_replace( 'Howdy,', 'Logged in as', $my_account->title );            
    $wp_admin_bar->add_node( array(
        'id' => 'my-account',
        'title' => $newtitle,
    ) );
}
add_filter( 'admin_bar_menu', 'replace_howdy',25 );

// Move Yoast to bottom
function yoasttobottom() {
	return 'low';
}
add_filter( 'wpseo_metabox_prio', 'yoasttobottom');
?>