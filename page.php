<?php get_header(); ?>

<div id="primary" class="site-content">
	<div id="content" role="main">

		<?php while ( have_posts() ) : the_post(); ?>

		<?php switch(get_the_ID()):
				case 5:
					get_template_part('template-parts/page/content','capabilities');
				break;
				case 66:
					get_template_part('template-parts/page/content','about');
				break;
				case 68:
					get_template_part('template-parts/page/content','safety-quality');
				break;
				case 70:
					get_template_part('template-parts/page/content','references');
				break;
				case 9:
					get_template_part('template-parts/page/content','contact');
				break;
				case 72:
					get_template_part('template-parts/page/content','careers');
				break;
				default:
					get_template_part('template-parts/page/content', 'page' ); ?>
			<?php endswitch; ?>

		<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->
</div><!-- #primary .site-content -->

<?php get_footer(); ?>