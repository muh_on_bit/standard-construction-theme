jQuery(function ($) {
  $('.plus_link_white').prepend("<svg class=\"mr-2\" width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n\t<path d=\"M12.7734 5.74219C12.8613 5.74219 12.9492 5.80078 13.0078 5.85938C13.0664 5.91797 13.125 6.00586 13.125 6.09375V7.03125C13.125 7.14844 13.0664 7.23633 13.0078 7.29492C12.9492 7.35352 12.8613 7.38281 12.7734 7.38281H7.38281V12.7734C7.38281 12.8906 7.32422 12.9785 7.26562 13.0371C7.20703 13.0957 7.11914 13.125 7.03125 13.125H6.09375C5.97656 13.125 5.88867 13.0957 5.83008 13.0371C5.77148 12.9785 5.74219 12.8906 5.74219 12.7734V7.38281H0.351562C0.234375 7.38281 0.146484 7.35352 0.0878906 7.29492C0.0292969 7.23633 0 7.14844 0 7.03125V6.09375C0 6.00586 0.0292969 5.91797 0.0878906 5.85938C0.146484 5.80078 0.234375 5.74219 0.351562 5.74219H5.74219V0.351562C5.74219 0.263672 5.77148 0.175781 5.83008 0.117188C5.88867 0.0585938 5.97656 0 6.09375 0H7.03125C7.11914 0 7.20703 0.0585938 7.26562 0.117188C7.32422 0.175781 7.38281 0.263672 7.38281 0.351562V5.74219H12.7734Z\" fill=\"white\"/>\n\t</svg>");
  $('.page_overlay').hide();
  $('#mobile-menu').on('click', function () {
    $('#nav-icon2').toggleClass('open');
    $('#nav').fadeToggle();
    $('#nav').toggleClass('navActive');
    $('.page_overlay').fadeToggle();
  });
  $('.page_overlay').on('click', function () {
    $('.page_overlay').fadeOut();
    $('#nav-icon2').removeClass('open');
    $('#nav').fadeOut();
    $('#nav').removeClass('navActive');
  });
  $('.plus_link').prepend("<svg class=\"mr-2\" width=\"14\" height=\"14\" viewBox=\"0 0 14 14\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n\t<path d=\"M12.7734 5.74219C12.8613 5.74219 12.9492 5.80078 13.0078 5.85938C13.0664 5.91797 13.125 6.00586 13.125 6.09375V7.03125C13.125 7.14844 13.0664 7.23633 13.0078 7.29492C12.9492 7.35352 12.8613 7.38281 12.7734 7.38281H7.38281V12.7734C7.38281 12.8906 7.32422 12.9785 7.26562 13.0371C7.20703 13.0957 7.11914 13.125 7.03125 13.125H6.09375C5.97656 13.125 5.88867 13.0957 5.83008 13.0371C5.77148 12.9785 5.74219 12.8906 5.74219 12.7734V7.38281H0.351562C0.234375 7.38281 0.146484 7.35352 0.0878906 7.29492C0.0292969 7.23633 0 7.14844 0 7.03125V6.09375C0 6.00586 0.0292969 5.91797 0.0878906 5.85938C0.146484 5.80078 0.234375 5.74219 0.351562 5.74219H5.74219V0.351562C5.74219 0.263672 5.77148 0.175781 5.83008 0.117188C5.88867 0.0585938 5.97656 0 6.09375 0H7.03125C7.11914 0 7.20703 0.0585938 7.26562 0.117188C7.32422 0.175781 7.38281 0.263672 7.38281 0.351562V5.74219H12.7734Z\" fill=\"#DE252A\"/>\n\t</svg>");
  $("<span class=\"nav_arrow\"><svg width=\"9\" height=\"6\" viewBox=\"0 0 9 6\" fill=\"none\" xmlns=\"http://www.w3.org/2000/svg\">\n\t<path d=\"M4.23438 4.98242H4.20117C4.26758 5.04883 4.36719 5.08203 4.5 5.08203C4.59961 5.08203 4.69922 5.04883 4.79883 4.98242L8.65039 1.06445C8.7168 0.998047 8.75 0.931641 8.75 0.798828C8.75 0.699219 8.7168 0.599609 8.65039 0.5L8.38477 0.267578C8.31836 0.201172 8.21875 0.167969 8.11914 0.167969C7.98633 0.167969 7.88672 0.201172 7.82031 0.267578L4.5 3.6543L1.17969 0.267578C1.08008 0.201172 0.980469 0.134766 0.880859 0.134766C0.748047 0.134766 0.681641 0.201172 0.615234 0.267578L0.349609 0.5C0.283203 0.599609 0.25 0.699219 0.25 0.798828C0.25 0.931641 0.283203 0.998047 0.382812 1.06445L4.23438 4.98242Z\" fill=\"white\"/>\n\t</svg></span>").insertAfter('.menu-item-has-children a:not(.sub-menu li a)');
  $('.nav_arrow').on('click', function () {
    $(this).parent().find('.sub-menu').toggle();
    $('.sub-menu').not($(this).parent().find('.sub-menu')).hide();
    $(this).toggleClass('rotate_arrow');
    $('.nav_arrow').not($(this)).removeClass('rotate_arrow');
  });
var settings = {
    slidesToShow: 6,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: true,
    dots: false,
    fade: false,
    // asNavFor: '.brands_slider',
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 800,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }]
  };
  var awardsSettings = {
    slidesToShow: 6,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: true,
    dots: false,
    fade: false,
    // asNavFor: '.awards_slider',
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3
      }
    }, {
      breakpoint: 800,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 600,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 480,
      settings: {
        slidesToShow: 1
      }
    }]
  };
  var settingsModal = {
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: false,
    arrows: true,
    dots: false,
    fade: false // asNavFor: '.awards_slider'

  };
  var settingsCapabilities = {
    slidesToShow: 2,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: true,
    dots: false,
    fade: false, // asNavFor: '.awards_slider'
    responsive: [{
      breakpoint: 1200,
      settings: {
        slidesToShow: 2
      }
    }, {
      breakpoint: 768,
      settings: {
        slidesToShow: 1
      }
    }]
  };
  var settingsProjectsFor = {
    slidesToShow: 1,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: true,
    dots: false,
    asNavFor: '.projects_inner_slider',
    fade: false // asNavFor: '.awards_slider'

  };
  var settingsProjects = {
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: false,
    asNavFor: '.projects_inner_slider_for',
    dots: false,
    centerMode: false,
    focusOnSelect: true,
    fade: false // asNavFor: '.awards_slider'

  };
  var settingsRelateds = {
    slidesToShow: 4,
    slidesToScroll: 1,
    infinite: true,
    autoplay: true,
    arrows: true,
    dots: false,
    fade: false 

  };
  var slickbrands = $('.brands_slider').slick(settings);
  var slickbrands = $('.awards_slider').slick(awardsSettings);
  var slickbrands_modal = $('.awards_slider_modal').slick(settingsModal);
  var sliderCapabilities = $('.capabilities_slider').slick(settingsCapabilities);
  
  var sliderProjects = $('.projects_inner_slider_for').slick(settingsProjectsFor);
  var sliderProjects = $('.projects_inner_slider').slick(settingsProjects);

  var sliderRelateds = $('.relatedpostslider').slick(settingsRelateds);
  $('.awards_slider img').on('click', function () {
    var sliderIndex = $(this).attr('data-ind') - 1;
    $('.awards_slider_modal').slick('slickGoTo', sliderIndex);
    console.log(sliderIndex);
  });

  if ($('body').hasClass('ios')) {
    $('INSERT SELECTOR HERE').on('click touchend', function () {
      var el = $(this);
      var link = el.attr('href');
      window.location = link;
    });
  }
});