<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content after
 *
 * @package bigsplash
 * @since bigsplash 1.0
 */
?>

	</div><!-- #main -->

	<div class="footer_nav py-5 inner">
		<div class="container-fluid py-4 my-4 my-sm-0 innerin">
			<div class="row">
				<div class="col-sm-6 col-lg-3 pb-5 pb-lg-0">
					<a class="font-light footer-title-fonts" href="/company/about-standard"><h3 class="h2 pb-2 font-light text-uppercase font-teko">Company</h3></a>
					<?php wp_nav_menu(array('theme_location' => 'company')); ?>
				</div>
				<div class="col-sm-6 col-lg-3 pb-5 pb-lg-0">
					<a class="font-light footer-title-fonts" href="/capabilities"><h3 class="h2 pb-2 font-light text-uppercase font-teko">Capabilities</h3></a>
					<?php wp_nav_menu(array('theme_location' => 'capabilities')); ?>
				</div>
				<div class="col-sm-6 col-lg-3 pb-5 pb-sm-0">
					<a class="font-light footer-title-fonts" href="/contact/#locations-sec"><h3 class="h2 pb-2 font-light text-uppercase font-teko">Offices</h3></a>
					<?php wp_nav_menu(array('theme_location' => 'offices')); ?>
				</div>
				<div class="col-sm-6 col-lg-3">
					<a class="font-light footer-title-fonts" href="/contact"><h3 class="h2 pb-2 font-light text-uppercase font-teko">Contact</h3></a>
					<strong>Corporate office:</strong>
					<p class="mb-0">9208 Winkler Dr</p>
					<p>Houston, Texas 77017</p>
					<p class="mb-0"><strong>Phone:  </strong> <a class="text_black" href="tel:(713) 941-1232">(713) 941-1232</a></p>
					<p><strong>Fax:  </strong> (713) 941-6616</p>
				</div>
			</div>
		</div>
	</div>

	<footer id="colophon" class="bg_black site-footer container-fluid py-4 py-sm-3">
		<div class="site-info row">
			<div class="copy col-12 text-white text-center">
				<p class="mb-0">&copy; <?= date("Y"); ?> <?php bloginfo( 'name' ); ?> <span class="d-none d-sm-inline">|</span> <span class="d-sm-inline d-block"><a class="text-white" target="_blank" href="https://www.bigsplashwebdesign.com/web-design/">Custom Web Design</a> by <a class="text-white" target="_blank" href="https://www.bigsplashwebdesign.com/">Big Splash Web Design & Marketing</a></span></p>
			</div>
		</div><!-- .site-info -->
	</footer><!-- #colophon .site-footer -->
</div><!-- #page .hfeed .site -->

<?php wp_footer(); ?>
<div class="page_overlay"></div>
</body>
</html>
