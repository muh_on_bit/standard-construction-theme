<?php

/**
Template Name: Capabilities
 */

get_header(); ?>

<?php $args = array(
  'post_type' => 'page',
  'posts_per_page' => -1,
  'meta_query' => array(
    array(
      'key' => '_wp_page_template',
      'value' => 'page-capabilities.php'
    )
  ),
  'order' => 'ASC'
);
$the_pages = new WP_Query($args); ?>

<div id="page-wrapper" class="bg_off_white pt-5">
  <div id="primary" class="site-content one-column">
    <div id="content" role="main">
      <?php while (have_posts()) : the_post(); ?>

        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
          <div class="entry-content innerin py-5 container-fluid">

            <div class="row mb-5 pb-5">
              <div class="offset-md-1 col-md-10 offset-xl-0 col-xl-3 border-right bor-mobile-hidden bor-below-mobile pr-0">
                <?php if ($the_pages->have_posts()) { ?>
                  <p class="h1 font-teko mb-0 font-light service-head">Services</p>
                  <hr class="mt-0 mb-4 separater-desktop">
                  <ul class="list-unstyled sevices-list">
                    <?php while ($the_pages->have_posts()) {
                      $the_pages->the_post(); ?>
                      <li class="mb-3">
                        <a class="plus_link" href="<?= get_the_permalink(); ?>">
                          <?= get_the_title(); ?>
                        </a>
                      </li>
                    <?php } ?>
                  </ul>
                <?php }
                wp_reset_postdata(); ?>
              </div>
              <div class="col-md-9 pb-5 pt-4 pl-4 pr-xl-5">
                <div class="pr-xl-4">
                  <?php the_content(); ?>
                </div>
              </div>
            </div>
          </div><!-- .entry-content -->

          <div class="safety_quality bg_off_white">
            <?php if (have_rows('two_column_list')) : ?>
              <?php while (have_rows('two_column_list')) : the_row(); ?>

                <div class="container-fluid pl-xl-0">
                  <div class="row justify-content-between">
                    <div class="col-md-12 col-xl-5 col-lg-3 p-0">
                      <img class="" src="<?= get_sub_field('image')['url']; ?>" alt="">
                    </div>
                    <div class="offset-md-1 col-md-10 offset-xl-0 col-xl-7 offset-lg-0 col-lg-9 px-3 px-lg-5 pt-5 bg_white">
                      <h2 class="px-3 py-4 pr-xl-5 mr-xl-5 font-teko font-medium font-48"><?= get_sub_field('title'); ?></h2>
                      <ul class="px-3 list-unstyled pb-4 mb-0 d-flex flex-column flex-wrap resp-wrap" style="height: 620px;">
                        <?php if (have_rows('list')) : ?>
                          <?php while (have_rows('list')) : the_row(); ?>

                            <li class="mb-3 pr-4 d-flex" style="width: 50%;">
                              <svg class="mr-3 position-relative" style="min-width: 22px; top: 11px;" width="22" height="2" viewBox="0 0 22 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect width="22" height="2" fill="#DE252A" />
                              </svg>
                              <?= get_sub_field('list_item'); ?>
                            </li>

                          <?php endwhile; ?>
                        <?php endif; ?>
                      </ul>
                    </div>
                  </div>
                </div>

              <?php endwhile; ?>
            <?php endif; ?>
          </div>

          <div class="container-fluid innerin py-5">
            <div class="capabilities_slider px-4">
              <?php if (have_rows('image_slider')) : ?>
                <?php while (have_rows('image_slider')) : the_row(); ?>

                  <div class="px-2 px-md-3">
                    <div class="slide" style="background-image: url(<?= get_sub_field('slide')['url']; ?>);"></div>
                    <p class="text-center font-20 mt-4 font-bold"><?= get_sub_field('slide')['caption']; ?></p>
                  </div>

                <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <div class="featured_project py-3 my-5">
            <div class="container-fluid innerin py-3-lg text-center-lg">
              <div class="row align-items-center justify-content-between">
                <?php if (have_rows('project_section')) : ?>
                  <?php while (have_rows('project_section')) : the_row(); ?>

                    <div class="col-md-12 offset-lg-1 col-lg-10 offset-xl-0 col-xl-4 pr-xl-0">
                      <p class="mb-0 font-14 mb-0 font-medium letter-space text-uppercase"><?= get_sub_field('subtitle'); ?></p>
                      <h4 class="text-white font-teko mb-3 h1 font-light text-uppercase"><?= get_sub_field('title'); ?></h4>
                      <p class="py-3 text-white"><?= get_sub_field('text'); ?></p>
                      <a class="plus_link" href="<?= get_sub_field('link'); ?>">View Projects</a>
                    </div>

                    <div class="col-md-12 offset-lg-1 col-lg-10 offset-xl-0 col-xl-7">
                      <img style="top: 50px;" class="overflow-lg position-relative img-fluid w-md-100" src="<?= get_sub_field('image')['url']; ?>" alt="">
                    </div>

                  <?php endwhile; ?>
                <?php endif; ?>
              </div>
            </div>
          </div>

          <div class="container-fluid innerin py-5 mt-5 px-xl-5">
            <h6 class="h1 font-teko mt-5 mb-5 text-uppercase text-center font-light">Just a few of our clients:</h6>
            <div class="awards_slider">
              <?php if (have_rows('clients_slider')) : ?>
                <?php $slideIndex = 0; ?>
                <?php while (have_rows('clients_slider')) : the_row(); ?>

                  <div class="d-flex align-items-center justify-content-center" style="min-height: 200px">
                    <img class="mx-auto" src="<?= get_sub_field('image')['url']; ?>" alt="">
                  </div>

                <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

          <div class="bg_red_dark py-4">
            <div class="d-flex flex-wrap justify-content-center align-items-center px-3 px-lg-0 py-2">
              <p class="font-teko mt-1 font-light h1 text-white text-uppercase pr-5 mb-0"><?php the_field('cta_text', 'option'); ?></p>
              <a class="button outlined_white" href="<?php the_field('cta_link', 'option'); ?>"><?php the_field('cta_button_text', 'option'); ?></a>
            </div>
          </div>

        </article><!-- #post-<?php the_ID(); ?> -->

      <?php endwhile; // end of the loop. 
      ?>
    </div><!-- #content -->
  </div><!-- #primary .site-content -->
</div>
<?php get_footer(); ?>