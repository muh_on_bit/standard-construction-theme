<?php
global $post;
$postcat = get_the_category( $post->ID );
$current_cat = $postcat[0]->cat_ID;

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<div class="entry-content">
		<div class="container-fluid innerin py-5 my-xl-5 my-0">
			 <div class="row">
	              <div class="col-md-3 border-right pr-4">
	                  <p class="h1 font-teko mb-0 font-light">Project Specs</p>
	                  <hr class="mt-0 mb-4">
	                  <ul class="list-unstyled">
	                      <li class="mb-3">
	                      	<?php if( get_field('project_specs') ): ?>
							    <?php while( the_repeater_field('project_specs') ): ?>
							        <p style="font-weight: 600;"><?php the_sub_field('spec_label'); ?></p>
							        <p><?php the_sub_field('spec_value'); ?></p>
							    <?php endwhile; ?>
							 <?php endif; ?>
	                      </li>
	                  </ul>
	              </div>
	              <div class="col-md-9 pb-5 pt-4 pl-xl-4 pr-xl-5">
	                <div class="pr-xl-4 pl-xl-5">
	                  <div class="projects_inner_slider_for px-4 mb-xl-4 mb-2">

			              <?php if (have_rows('slider_images')) {  ?>
			                <?php while (have_rows('slider_images')) : the_row(); ?>

			                  <div class="px-3">
			                    <div class="slide" style="background-image: url(<?= get_sub_field('slides')['url']; ?>);"></div>
			                    <?php if(count(get_field('slider_images'), 0) < 1) {break;}?>
			                  </div>

			                <?php endwhile; ?>
			              <?php }
			              else {
			              	?>
			              		<div>Sorry! No images are found for this Project.</div>
			              	<?php
			              } ?>
			            </div>

			            <div class="projects_inner_slider px-4">
			              <?php if(have_rows('slider_images') && count(get_field('slider_images'), 0) > 1) { ?>
			                <?php while (have_rows('slider_images')) : the_row(); ?>

			                  <div class="px-xl-3 px-1">
			                    <div class="slide" style="background-image: url(<?= get_sub_field('slides')['url']; ?>);"></div>
			                  </div>

			                <?php endwhile; ?>
			              <?php } ?>
			            </div>
			           
	                </div>
	              </div>
	            </div>
	          </div>

			<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:', 'bigsplash' ), 'after' => '</div>' ) ); ?>
			</div>

			<div class="related-section py-xl-5 py-0">
				 <div class="container-fluid innerin py-5">
				 	<div class="row px-xl-0 px-3 ">
				 		<p class="h1 font-teko mb-4 font-light w-100">Related Projects</p>
				 	</div>
				 	<div class="row px-xl-0 px-3">
				 		
	            	<?php

					//query arguments
					$args = array(
					    'post_type' => 'projects',
					    'post_status' => 'publish',
					    'posts_per_page' => -1,
					    'orderby' => 'rand',
					    'post__not_in' => array ($post->ID),
					    'cat' => $current_cat,
					);

					//the query
					$relatedPosts = new WP_Query( $args );

					//loop through query
					if($relatedPosts->have_posts()){
					    echo '<ul class="relatedpostslider list-unstyled list-wrap" style="width: 100%;">';
					    while($relatedPosts->have_posts()){ 
					        $relatedPosts->the_post();
					?>
					        <li class="slide post-thumbnail" style=""><a class="mt-3 rel-font d-block" href="<?php the_permalink(); ?>">
					        	<?php
					        		if(has_post_thumbnail()) {
			                            the_post_thumbnail('full');
			                        }
					        	?>
					        		</a>
					        	<a class="mt-3 rel-font d-block" href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					        </li>
					<?php
					    }
					    echo '</ul>';
					}else{
					    echo 'Sorry! No related posts found.';
					}

					//restore original post data
					wp_reset_postdata();

					?>
				 	</div>
				</div>
            </div>

	</div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->