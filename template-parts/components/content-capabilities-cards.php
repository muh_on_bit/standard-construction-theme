<div class="card-columns card-column-new">
  <?php if (have_rows('services')) : ?>
    <?php $count = 0; ?>
    <?php while (have_rows('services')) : the_row();
      $count++; ?>

      <div class="card card-new item<?= $count; ?> d-flex justify-content-end border-0 mb-5 mb-md-0 rounded-0" style="background: linear-gradient(180deg, rgba(43, 43, 43, 0) 60.34%, rgba(43, 43, 43, 0.9) 100%), url('<?= get_sub_field('image')['url']; ?>')">
        <div class="card-content">
          <h3 class="h1 font-light text-uppercase text-white font-teko"><?= get_sub_field('title'); ?></h3>
          <p class="mb-0 text-white"><?= get_sub_field('text'); ?></p>
        </div>
        <div class="card-overlay"></div>
        <a href="<?= get_sub_field('link'); ?>">
          <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M25.5469 12.0703C25.7227 12.0703 25.8984 12.1875 26.0156 12.3047C26.1328 12.4219 26.25 12.5977 26.25 12.7734V13.4766C26.25 13.7109 26.1328 13.8867 26.0156 14.0039C25.8984 14.1211 25.7227 14.1797 25.5469 14.1797H14.1797V25.5469C14.1797 25.7812 14.0625 25.957 13.9453 26.0742C13.8281 26.1914 13.6523 26.25 13.4766 26.25H12.7734C12.5391 26.25 12.3633 26.1914 12.2461 26.0742C12.1289 25.957 12.0703 25.7812 12.0703 25.5469V14.1797H0.703125C0.46875 14.1797 0.292969 14.1211 0.175781 14.0039C0.0585938 13.8867 0 13.7109 0 13.4766V12.7734C0 12.5977 0.0585938 12.4219 0.175781 12.3047C0.292969 12.1875 0.46875 12.0703 0.703125 12.0703H12.0703V0.703125C12.0703 0.527344 12.1289 0.351562 12.2461 0.234375C12.3633 0.117188 12.5391 0 12.7734 0H13.4766C13.6523 0 13.8281 0.117188 13.9453 0.234375C14.0625 0.351562 14.1797 0.527344 14.1797 0.703125V12.0703H25.5469Z" fill="white" />
          </svg>
        </a>
      </div>

    <?php endwhile; ?>
  <?php endif; ?>
</div>
<div style="clear: both;"></div>