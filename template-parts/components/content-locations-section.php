<div id="locations-sec" class="inner locations_section py-sm-5">
  <div class="container-fluid py-5">
    <div class="row py-4 justify-content-end">
      <?php if (have_rows('locations', 1)) : ?>
        <?php while (have_rows('locations', 1)) : the_row(); ?>

          <div class="col-lg-6 text-center text-sm-left">
            <h1 class="font-teko text-uppercase pt-3 font-72 font-bold">
              <span class="text-white"><?= get_sub_field('title_white'); ?></span>
              <span class="text_red"><?= get_sub_field('title_red'); ?></span>
            </h1>

            <div class="row">
              <?php if (have_rows('location')) : ?>
                <?php while (have_rows('location')) : the_row(); ?>

                  <div class="col-sm-6 pt-5 text-white locations-wrap">
                    <h2 class="font-regular font-teko"><?= get_sub_field('title'); ?></h2>
                    <div>
                      <?= get_sub_field('info'); ?>
                    </div>
                  </div>

                <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>

        <?php endwhile; ?>
      <?php endif; ?>

    </div>
  </div>
</div>