<div class="services_section inner bg_off_white pt-5 pb-3">
  <div class="innerin container-fluid mt-5 mt-sm-0 mt-lg-5 mb-md-5 py-sm-5 pb-5 mb-sm-0">
    <?php if (have_rows('services_section', 1)) : ?>
      <?php while (have_rows('services_section', 1)) : the_row(); ?>

        <?php if (is_page(1)) { ?>
          <h1 class="font-teko pb-5 pb-sm-0 text-uppercase text-center pt-sm-5 font-72 font-bold">
            <span class="text_black"><?= get_sub_field('title_black'); ?></span>
            <span class="text_red"><?= get_sub_field('title_red'); ?></span>
          </h1>

          <p class="text-center pb-5 d-none d-sm-block mx-auto" style="max-width: 810px;"><?= get_sub_field('intro'); ?></p>
        <?php } ?>

        <?php get_template_part('template-parts/components/content', 'capabilities-cards'); ?>

      <?php endwhile; ?>
    <?php endif; ?>
  </div>
</div>