<div class="jobs-wrap">
    <?php
    $args = array(
        'post_type' => 'jobs',
        'posts_per_page' => -1,
        'post_status' => 'publish',
        'order' => 'ASC',
    );
    $query = new WP_Query($args);
      if ($query->have_posts() ) : 
        while ( $query->have_posts() ) : $query->the_post();
          $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
          $projContent = get_the_excerpt();
          $strippedcont = strip_tags($projContent, '<p> <a>'); //replace <p> and <a> with whatever tags you want to keep after the strip
          ?>
           <div class="accordion accord-font py-5 px-0">
            <div class="accord-inner d-flex px-4">
              <span><strong><?= the_title(); ?></strong></span>
              <span class="job_location"><?= get_field('job_location'); ?></span>
              <span class="accord-btn">Learn More</span><!-- <span class="accord-button">x</span> -->
            </div>
          </div>
          <div class="panel px-0">
            <div class="panel-inner py-5 px-4">
            <p class="h1 font-teko mb-3 font-light uppercase">Job description</p>
            <p class="mb-5"><?= get_field('job_description'); ?></p>
            <p class="h1 font-teko mb-3 font-light uppercase">Responsibilities</p>
            <ul class="pl-5 ml-3 pb-3">
               <?php if (have_rows('job_responsibilities')) : ?>
                    <?php while (have_rows('job_responsibilities')) : the_row(); ?>

                      <li class="cust-list-style px-3 py-3">
                          <?= get_sub_field('job_requirement'); ?>
                      </li>

                    <?php endwhile; ?>
                  <?php endif; ?>
                </div>
                <div class="accord-submit-section p-5 text-center">
                  <p class="h1 font-teko mb-3 font-light uppercase text-center">Ready to apply?</p>
                  <a id="get_started" class="button cust-btn uppercase text-center" href="?position=<?= the_title(); ?>#form-sec">Get started</a>
                </div>
                <hr class="divider_job">
            </ul>
            </div>


            
            <?php
        endwhile;
        wp_reset_postdata();
      else:
        echo "There are currently no available positions. Please check back regularly for updates.";
      endif;
      ?>
   
    <script>
 	jQuery( document ).ready(function() {
        jQuery("body").on('click', '.accord-btn',function(){
          var current_div = jQuery(this).closest('.accordion').next('.panel');
          jQuery('.accordion').removeClass('active');
          if(current_div.css('min-height') == "0px"){
            jQuery('.accordion').next('.panel').css('min-height', 0 );
            jQuery(this).closest('.accordion').addClass('active');
            current_div.css('min-height', current_div.prop('scrollHeight')+'px' );
          }  
          else{
            jQuery('.accordion').next('.panel').css('min-height', 0 );
            current_div.css('min-height', 0 );
          }
        });


        jQuery("#get_started").click(function() {
          jQuery('html, body').animate({
              scrollTop: $("#form-sec").offset().top
          }, 2000);
        });
      });
      </script>
</div>