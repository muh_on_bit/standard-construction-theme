<div class="card-columns projects-wrap">

    <?php
      $args = array(
              'post_type' => 'projects',
              'posts_per_page' => -1,
              'post_status' => 'publish',
              'order' => 'ASC',
          );
      $query = new WP_Query($args);
      if ($query->have_posts() ) : 
          while ( $query->have_posts() ) : $query->the_post();
            $featured_img_url = get_the_post_thumbnail_url(get_the_ID(),'full');
            $projContent = get_the_excerpt();
            $strippedcont = strip_tags($projContent, '<p> <a>'); //replace <p> and <a> with whatever tags you want to keep after the strip
            ?>
             <div class="card item<?php //$count; ?> d-flex justify-content-end border-0 mb-5 mb-md-0 rounded-0" style="background: linear-gradient(180deg, rgba(43, 43, 43, 0) 60.34%, rgba(43, 43, 43, 0.9) 100%), url('<?php echo($featured_img_url); ?>')">
              <div class="card-content">
                <h3 class="mb-0 h1 font-light text-white font-teko"><?= the_title(); ?></h3>
                <p class="mb-2 text-white medium-italic"><?php if( get_field('project_subtitle') ) { ?>
          <?php echo get_field('project_subtitle'); ?>
        <?php } ?></p>
                <p class="card-desc text-white"><?php echo $strippedcont; ?></p>
              </div>
              <div class="card-overlay"></div>
                <a href="javascript:void(0)<?php// the_permalink(); ?>" class="disabled_link">
                  <svg width="27" height="27" viewBox="0 0 27 27" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M25.5469 12.0703C25.7227 12.0703 25.8984 12.1875 26.0156 12.3047C26.1328 12.4219 26.25 12.5977 26.25 12.7734V13.4766C26.25 13.7109 26.1328 13.8867 26.0156 14.0039C25.8984 14.1211 25.7227 14.1797 25.5469 14.1797H14.1797V25.5469C14.1797 25.7812 14.0625 25.957 13.9453 26.0742C13.8281 26.1914 13.6523 26.25 13.4766 26.25H12.7734C12.5391 26.25 12.3633 26.1914 12.2461 26.0742C12.1289 25.957 12.0703 25.7812 12.0703 25.5469V14.1797H0.703125C0.46875 14.1797 0.292969 14.1211 0.175781 14.0039C0.0585938 13.8867 0 13.7109 0 13.4766V12.7734C0 12.5977 0.0585938 12.4219 0.175781 12.3047C0.292969 12.1875 0.46875 12.0703 0.703125 12.0703H12.0703V0.703125C12.0703 0.527344 12.1289 0.351562 12.2461 0.234375C12.3633 0.117188 12.5391 0 12.7734 0H13.4766C13.6523 0 13.8281 0.117188 13.9453 0.234375C14.0625 0.351562 14.1797 0.527344 14.1797 0.703125V12.0703H25.5469Z" fill="white" />
                  </svg>
                </a>
            </div>
            <?php
          endwhile;
          wp_reset_postdata();
      endif;
    ?>

</div>