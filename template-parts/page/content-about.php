<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content">
    <div class="about_cont">
      <div class="container-fluid innerin">
        <div class="row">
          <div class="col-md-8">
            <?php the_content(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="about_cta bg_red_dark py-5 text-center text-white">
      <?php if (have_rows('call_to_action')) : ?>
        <?php while (have_rows('call_to_action')) : the_row(); ?>

          <h3 class="font-teko font-48 font-medium"><?= get_sub_field('title'); ?><a href="<?= get_field('cta_title_link', 'option'); ?>" class="text-white"><span class="bg_black d-inline-block pt-1 ml-2 px-1"><?= get_sub_field('title_cta'); ?></span></a></h3>
          <a class="button outlined_white mt-3" href="<?= get_sub_field('button_link'); ?>">
            <?= get_sub_field('button_text'); ?>
          </a>

        <?php endwhile; ?>
      <?php endif; ?>
    </div>

    <div class="our_clients_section bg_off_white">
      <?php if (have_rows('our_clients')) : ?>
        <?php while (have_rows('our_clients')) : the_row(); ?>

          <div class="container-fluid pl-xl-0">
            <div class="row justify-content-between">
              <div class="col-md-12 col-xl-4 col-lg-3 p-0">
                <img src="<?= get_sub_field('image')['url']; ?>" alt="">
              </div>
              <div class="offset-md-1 col-md-10 offset-xl-0 col-xl-8 offset-lg-0 col-lg-9  p-5 bg_white">
              <h4 class="text-uppercase px-3 pb-4 font-teko font-light h1"><?= get_sub_field('title'); ?></h4>
                <ul class="row px-3 list-unstyled mb-0">
                  <?php if (have_rows('list')) : ?>
                    <?php while (have_rows('list')) : the_row(); ?>

                      <li class="col-sm-6 col-md-6 col-lg-4 col-xl-4 mb-3 d-flex align-items-center">
                        <svg class="mr-3" width="22" height="2" viewBox="0 0 22 2" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <rect width="22" height="2" fill="#DE252A" />
                        </svg>
                        <?= get_sub_field('list_item'); ?>
                      </li>

                    <?php endwhile; ?>
                  <?php endif; ?>
                </ul>
              </div>
            </div>
          </div>

        <?php endwhile; ?>
      <?php endif; ?>
    </div>
    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'bigsplash'), 'after' => '</div>')); ?>
  </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->