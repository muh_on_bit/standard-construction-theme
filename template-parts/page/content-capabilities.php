<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content">
    <?php get_template_part('template-parts/components/content', 'capabilities-section'); ?>

    <div class="cpblts_callout bg_red_dark">
      <?php if (have_rows('services_brochures')) : ?>
        <?php while (have_rows('services_brochures')) : the_row(); ?>

          <div class="container-fluid innerin">
            <div class="row align-items-center py-5">
              <div class="col-12 col-md-12 col-xl-6 service-heading" >
                <p class="font-teko mb-0 font-medium font-48 text-white"><?= get_sub_field('title'); ?></p>
              </div>
              <div class="col-12 offset-md-2 col-md-8 offset-xl-0 col-xl-6">
                <div class="row">
                  <?php if (have_rows('brochures')) : ?>
                    <?php while (have_rows('brochures')) : the_row(); ?>

                      <div class="col-md-6 pb-4">
                        <a class="plus_link_white" target="_blank" rel="noreferrer noopener" href="<?= get_sub_field('file')['url']; ?>">
                          <?= get_sub_field('title'); ?>
                        </a>
                      </div>

                    <?php endwhile; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>

        <?php endwhile; ?>
      <?php endif; ?>
    </div>
    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'bigsplash'), 'after' => '</div>')); ?>
  </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->