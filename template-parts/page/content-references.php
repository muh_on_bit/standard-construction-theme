<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content">
    <div class="container-fluid py-5 innerin pr-xl-5">
      <div class="row py-5 pr-xl-5">
        <div class="col-md-9 pr-xl-5">
          <?php the_content(); ?>
        </div>
      </div>
    </div>

    <div class="references py-5">
      <div class="container-fluid innerin py-5">
        <h2 class="font-teko pb-5 mx-auto font-48 font-medium text-center" style="max-width: 850px;">
          <?= get_field('list_title'); ?>
        </h2>
        <div class="row">
          <?php if (have_rows('list')) : ?>
            <?php while (have_rows('list')) : the_row(); ?>

              <div class="col-md-6 col-lg-4 pl-xl-5 pb-3">
                <div class="pl-xl-5">
                  <?= get_sub_field('list_item'); ?>
                </div>
              </div>

            <?php endwhile; ?>
          <?php endif; ?>
        </div>
      </div>
    </div>
    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'bigsplash'), 'after' => '</div>')); ?>
  </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->