<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content bg_off_white py-5 position-relative">
    <div class="form_section">
      <div class="container-fluid innerin py-5">
        <div class="row pb-5">
          <div class="col-lg-9 col-xl-7 px-xl-0">
            <?= get_field('career_description'); ?>

            <div class="py-5 mt-5 mb-3">
              <h3 class="pt-3 font-teko font-48 font-medium">Open Positions</h3>
            </div>

            <?php get_template_part('template-parts/components/content', 'job-accordion'); ?>

          </div>
          <div class="col-lg-9 col-xl-5 position-static">
            <ul class="contact_sidebar bg_white px-5 list-unstyled mb-0">
              <?php if (have_rows('sidebar')) : ?>
                <?php $counter = 0; ?>
                <?php while (have_rows('sidebar')) : the_row();
                  $counter++; ?>

                  <?php if ($counter !== 1) { ?>
                    <hr class="my-0">
                  <?php } ?>
                  <li class="py-5">
                    <a class="text_red font-teko font-light h1" href="<?= get_sub_field('link'); ?>">
                      <?= get_sub_field('link_text'); ?>
                      <svg class="ml-2" width="14" height="14" viewBox="0 0 14 14" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M6.38672 0.713867C6.44531 0.655273 6.5332 0.625977 6.65039 0.625977C6.73828 0.625977 6.82617 0.655273 6.88477 0.713867L13.0078 6.86621C13.0664 6.9248 13.125 7.0127 13.125 7.10059C13.125 7.21777 13.0664 7.27637 13.0078 7.33496L6.88477 13.4873C6.82617 13.5459 6.73828 13.5752 6.65039 13.5752C6.5332 13.5752 6.44531 13.5459 6.38672 13.4873L5.80078 12.9014C5.74219 12.8428 5.71289 12.7842 5.71289 12.667C5.71289 12.5791 5.74219 12.4912 5.80078 12.4033L10.3418 7.8623H0.351562C0.234375 7.8623 0.146484 7.83301 0.0878906 7.77441C0.0292969 7.71582 0 7.62793 0 7.51074V6.69043C0 6.60254 0.0292969 6.51465 0.0878906 6.45605C0.146484 6.39746 0.234375 6.33887 0.351562 6.33887H10.3418L5.80078 1.79785C5.74219 1.73926 5.71289 1.65137 5.71289 1.53418C5.71289 1.44629 5.74219 1.3584 5.80078 1.2998L6.38672 0.713867Z" fill="#DE252A" />
                      </svg>
                    </a>
                  </li>


                <?php endwhile; ?>
              <?php endif; ?>
            </ul>
          </div>
        </div>
      </div>
       <div id="form-sec" class="careers_form">
          <div class="careers-inner py-5">
            <div class="container-fluid innerin">
              <div class="pb-1 text-center">
                <div class="pb-4 text-center w-100">
                  <div class="career-heading-width">
                    <h3 class="pt-3 font-teko font-48 font-medium"><?= get_field('careers_form_title'); ?></h3>
<!--                     <p><?= get_field('careers_form_description'); ?></p> -->
                  </div>
                </div>
                <div class="career-form-width">
                  <?php echo do_shortcode( '[gravityform id="2" title="false"]' ); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
    </div>

    
    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'bigsplash'), 'after' => '</div>')); ?>
  </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->