<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content">
    <?php get_template_part('template-parts/components/content', 'projects-section'); ?>

    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'bigsplash'), 'after' => '</div>')); ?>
  </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->