<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

  <div class="entry-content bg_off_white">
    <div class="container-fluid py-5 innerin">
      <div class="row py-5">
        <div class="col-md-8">
          <?php the_content(); ?>
        </div>
      </div>
    </div>

    <div class="safety_quality mb-5">
      <div class="container-fluid pl-xl-0">
        <div class="row justify-content-between">
          <div class="col-md-12 col-xl-5 col-lg-3 p-0">
            <img src="<?= get_field('image')['url']; ?>" alt="">
          </div>
          <div class="offset-md-1 col-md-10 offset-xl-0 col-xl-7 offset-lg-0 col-lg-9 px-5 px-lg-5 pt-5 bg_white">
            <h4 class="text-uppercase px-3 pb-4 font-teko font-light h1"><?= get_sub_field('title'); ?></h4>
            <div class="row px-3 mb-0">
              <?php $count = 0;
              if (have_rows('safety_info')) : ?>
                <?php while (have_rows('safety_info')) : the_row();
                  $count++; ?>
                  <?php $image = get_sub_field('image')['url']; ?>

                  <?php if ($image) { ?>
                    <div class="col-md-3 pr-xl-4 pr-0 px-0 <?= $count == 1 ? 'pb-5' : 'pt-5'; ?>">
                      <img class="img-fluid" src="<?= $image; ?>" alt="">
                    </div>
                  <?php } ?>

                  <div class="px-0 pr-0 <?= $image ? 'col-md-9 pr-xl-4' : ''; ?> <?= $count == 1 ? 'pb-5' : 'pt-5'; ?>">
                    <p class="text-uppercase font-light font-teko h1"><?= get_sub_field('title'); ?></p>
                    <div class="<?= $count == 1 ? 'pr-xl-2' : ''; ?>">
                      <p class="py-3 pr-xl-5 mr-xl-4"><?= get_sub_field('text'); ?></p>
                    </div>
                    <?php if (get_sub_field('button_text')) { ?>
                      <a class="button btn_red" href="<?= get_sub_field('button_link'); ?>">
                        <?= get_sub_field('button_text'); ?>
                      </a>
                    <?php } ?>
                  </div>

                <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <?php if (have_rows('awards_section')) : ?>
      <?php while (have_rows('awards_section')) : the_row(); ?>

        <div class="awards_section awards-wrapper py-5" style="background-image: url(<?= get_sub_field('background_image')['url']; ?>);">
          <div class="container-fluid py-3 innerin">
            <div class="row pb-5">
              <div class="col-md-8 text-white">
                <p class="font-14 mb-0 font-medium letter-space text-uppercase"><?= get_sub_field('subtitle'); ?></p>
                <h5 class="h1 pb-2 font-light font-teko text-uppercase"><?= get_sub_field('title'); ?></h5>
                <p><?= get_sub_field('text'); ?></p>
              </div>
            </div>
            <h6 class="h2 text-white pb-5 font-teko text-center font-semibold"><?= get_sub_field('slider_title'); ?></h6>
            <div class="awards_slider">
              <?php if (have_rows('slider')) : ?>
                <?php $slideIndex = 0; ?>
                <?php while (have_rows('slider')) : the_row();
                  $slideIndex++; ?>

                  <div>
                    <img data-ind="<?= $slideIndex; ?>" data-toggle="modal" data-target="#exampleModal" class="mx-auto" src="<?= get_sub_field('image')['url']; ?>" alt="">
                  </div>

                <?php endwhile; ?>
              <?php endif; ?>
            </div>
          </div>
        </div>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
              <div class="pt-2 pr-3">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <svg width="21" height="21" viewBox="0 0 21 21" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M12.1446 10.5L20.8102 19.1657C20.9367 19.3554 21 19.5452 21 19.7349C21 19.988 20.9367 20.1145 20.8102 20.241L20.241 20.8102C20.1145 20.9367 19.9247 21 19.7349 21C19.4819 21 19.2922 20.9367 19.1657 20.8102L10.5 12.0813L1.83434 20.8102C1.64458 20.9367 1.45482 21 1.26506 21C1.01205 21 0.885542 20.9367 0.759036 20.8102L0.189759 20.241C0.063253 20.1145 0 19.988 0 19.7349C0 19.5452 0.063253 19.3554 0.189759 19.1657L8.91867 10.5L0.189759 1.83434C0.063253 1.70783 0 1.51807 0 1.26506C0 1.0753 0.063253 0.885542 0.189759 0.759036L0.759036 0.189759C0.885542 0.063253 1.01205 0 1.26506 0C1.45482 0 1.64458 0.063253 1.83434 0.189759L10.5 8.85542L19.1657 0.189759C19.2922 0.063253 19.4819 0 19.7349 0C19.9247 0 20.1145 0.063253 20.241 0.189759L20.8102 0.759036C20.9367 0.885542 21 1.0753 21 1.26506C21 1.51807 20.9367 1.70783 20.8102 1.83434L12.1446 10.5Z" fill="#A3A2B3" />
                  </svg>
                </button>
              </div>
              <div class="modal-body p-5 container">
                <div class="awards_slider_modal">
                  <?php if (have_rows('slider')) : ?>
                    <?php while (have_rows('slider')) : the_row(); ?>

                      <div class="px-5 d-flex align-items-center modal-wrapper">
                        <img data-toggle="modal" data-target="#exampleModal" class="mx-auto modal-open" src="<?= get_sub_field('image')['url']; ?>" alt="">
                        <!-- <div class="pl-xl-4">
                          <p class="font-bold"><?= get_sub_field('modal_title'); ?></p>
                          <p class="pr-xl-5"><?= get_sub_field('modal_text'); ?></p>
                        </div> -->
                      </div>

                    <?php endwhile; ?>
                  <?php endif; ?>
                </div>
              </div>
            </div>
          </div>
        </div>

      <?php endwhile; ?>
    <?php endif; ?>


    <?php wp_link_pages(array('before' => '<div class="page-links">' . __('Pages:', 'bigsplash'), 'after' => '</div>')); ?>
  </div><!-- .entry-content -->
</article><!-- #post-<?php the_ID(); ?> -->